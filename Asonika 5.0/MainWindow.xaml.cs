﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel.Composition;
using GuiExtention;
using System.ComponentModel.Composition.Hosting;
using System.Windows.Forms;


namespace Asonika_5._0
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        [Import("Module1", typeof(IGuiExtention), AllowDefault = true)]
        IGuiExtention module1;

        [Import("Module2", typeof(IGuiExtention),AllowDefault =true)]
        IGuiExtention module2;
        public MainWindow()
        {
            InitializeComponent();
            Compose();
            if(module1!=null)
            {
                module1.setControls(dockManager, toolBarTray, menu);
                module1.addMenuItems(menu);
            }
            
            if(module2!=null)
            {
                module2.setControls(dockManager, toolBarTray, menu);
                module2.addMenuItems(menu);
            }
            //filesTree._tree.Model = new ProjectModel();
            //WinFormsWindow.IsVisible = false;

        }
        private void Compose()
        {
            if (System.IO.Directory.Exists(@"C:\x\Visual Studio 2015\Projects\ClassLibrary1\ClassLibrary1\bin\Release\") &&
                System.IO.Directory.Exists(@"C:\x\Visual Studio 2015\Projects\Module2\Module2\bin\Debug\"))
            {
                AggregateCatalog catalog_agg = new AggregateCatalog(new DirectoryCatalog(@"C:\x\Visual Studio 2015\Projects\ClassLibrary1\ClassLibrary1\bin\Release\", "ClassLibrary1.dll"),
                                                                    new DirectoryCatalog(@"C:\x\Visual Studio 2015\Projects\Module2\Module2\bin\Debug\", "Module2.dll"));
                CompositionContainer container = new CompositionContainer(catalog_agg);
                container.SatisfyImportsOnce(this);
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            if(dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                filesTree._tree.Model = new ProjectModel(dialog.SelectedPath);
        }
    }
}

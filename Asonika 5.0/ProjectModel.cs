﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aga.Controls.Tree;
namespace Asonika_5._0
{
    class ProjectModel : ITreeModel
    {
        Random gen;
        /// <summary>
        /// Заданный путь к проекту.
        /// </summary>
        string _path;
        string Path
        {
            get { return _path; }
            set { _path = value; }
        }
        /// <summary>
        /// Инициализирует новый экземпляр класса ProjectModel.
        /// </summary>
        /// <param name="path">Путь к каталогу с проектом.</param>
        public ProjectModel(string path)
        {
            if (!System.IO.Directory.Exists(path))
            {
                return;
            }
            Path = path;
            gen = new Random();
        }
        public ProjectModel()
        {
            //Для отладки
            Path = @"C:\x\Visual Studio 2015\Projects\ClassLibrary1\ClassLibrary1";
            gen = new Random();
        }
        /// <summary>
        /// Возвращает коллекцию потомков(файлы или каталоги) от parent.
        /// </summary>
        /// <param name="parent">Каталог, потомки которого возвращает данный метод.</param>
        /// <returns></returns>
        public IEnumerable GetChildren(object parent)
        {
            string current_directory;
            if (parent == null)
                current_directory = Path;
            else current_directory = ((NodeValue)parent).Path;
            foreach (string item in System.IO.Directory.GetDirectories(current_directory))
            {
                string directory = System.IO.Path.GetFileName(item);
                yield return new NodeValue()
                {
                    Name = directory,
                    Path = item,
                    IsFolder = true
                };
            }
            foreach (string item in System.IO.Directory.GetFiles(current_directory))
            {
                string file = System.IO.Path.GetFileName(item);
                string ext = System.IO.Path.GetExtension(file);
                bool b1 = (gen.Next(0, 1) == 0);
                bool b2 = (gen.Next(0, 1) == 0);
                yield return new NodeValue()
                {
                    Name = file,
                    NeedToSave = b1,
                    IsOpen = b2,
                    Extension = ext,
                    Path = item,
                    IsFolder = false
                };
                
            }
        }

        public bool HasChildren(object parent)
        {
            if (((NodeValue)parent).IsFolder)
                if (System.IO.Directory.GetDirectories(((NodeValue)parent).Path).Length > 0 || System.IO.Directory.GetFiles(((NodeValue)parent).Path).Length > 0)
                    return true;
            return false;
        }
    }
    /// <summary>
    /// Структура, включающая в себе данные о файле или каталоге.
    /// </summary>
    public struct NodeValue
    {
        /// <summary>
        /// Название файла или каталога.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// <c>C#</c> если файл был изменен, иначе <b>false</b>.
        /// </summary>
        public bool NeedToSave { get; set; }
        /// <summary>
        /// <c>true</c> если файл открыт, иначе <c>false</c>.
        /// </summary>
        public bool IsOpen { get; set; }
        /// <summary>
        /// Расширение для файла. Если это каталог, то значение принимает <c>null</c>.
        /// </summary>
        public string Extension { get; set; }
        /// <summary>
        /// <c>true</c> если это каталог, иначе <c>false</c>.
        /// </summary>
        public bool IsFolder { get; set; }
        /// <summary>
        /// Полное имя файла или каталога.
        /// </summary>
        public string Path { get; set; }
    }
}

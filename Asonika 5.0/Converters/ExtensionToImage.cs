﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Globalization;
namespace Asonika_5._0.Converters
{
    
    class ExtensionToImage : IValueConverter
    {
        public object Convert(object o, Type type, object parameter, CultureInfo culture)
        {
            if (((NodeValue)o).IsFolder) return "/icons/Folder3.png";
            switch (((NodeValue)o).Extension)
            {
                case ".cs":
                    return "/icons/Information.png";
                case ".csproj":
                    return "/icons/Database.png";
                case ".png":
                    return "/icons/Calendar.png";
                case ".xaml":
                    return "/icons/Link.png";
                default:
                    return "/icons/Help.png";
            }
        }


        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

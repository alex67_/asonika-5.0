﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Asonika_5._0
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class BifidWindow : UserControl
    {
        bool isUpperPanelHidden;
        bool isBottomPanelHidden;
        public Control upperPanel_;
        public static readonly DependencyProperty upperPanelProperty= DependencyProperty.Register("upperPanel", typeof(object), typeof(BifidWindow), new UIPropertyMetadata(null));
        public object upperPanel
        {
            get { return (object)GetValue(upperPanelProperty); }
            set { SetValue(upperPanelProperty, value);
                upperPanel_ = (Control)value;
            }
        }
        private Control bottomPanel_;
        public static readonly DependencyProperty bottomPanelProperty = DependencyProperty.Register("bottomPanel", typeof(object), typeof(BifidWindow), new UIPropertyMetadata(null));
        public object bottomPanel
        {
            get { return (object)GetValue(bottomPanelProperty); }
            set
            {
                SetValue(bottomPanelProperty, value);
                bottomPanel_ = (Control)value;
            }
        }
        public BifidWindow()
        {
            InitializeComponent();
            isBottomPanelHidden = false;
            isUpperPanelHidden = false;
        }
        void setUpperControl()
        {

        }
        void setBottomControl()
        {

        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            switch (button.Tag as string)
            {
                case "upper":
                    if (!isBottomPanelHidden)//hide the upper grid
                    {
                        upperGrid.Visibility = Visibility.Collapsed;
                        splitter.Visibility = Visibility.Collapsed;
                        reverseButtonImage(bottomButton);
                        isUpperPanelHidden = true;
                        mainGrid.RowDefinitions[0].Height = GridLength.Auto;
                    }
                    else//show the bottom grid
                    {
                        bottomGrid.Visibility = Visibility.Visible;
                        splitter.Visibility = Visibility.Visible;
                        reverseButtonImage(upperButton);
                        isBottomPanelHidden = false;
                        mainGrid.RowDefinitions[2].Height = GridLength.Auto;
                    }
                    break;
                case "bottom":
                    if (!isUpperPanelHidden)//hide the bottom grid
                    {
                        bottomGrid.Visibility = Visibility.Collapsed;
                        splitter.Visibility = Visibility.Collapsed;
                        reverseButtonImage(upperButton);
                        isBottomPanelHidden = true;
                        mainGrid.RowDefinitions[0].Height = new GridLength(3,GridUnitType.Star);
                        mainGrid.RowDefinitions[2].Height = new GridLength(0);
                    }
                    else//show the upper grid
                    {
                        upperGrid.Visibility = Visibility.Visible;
                        splitter.Visibility = Visibility.Visible;
                        reverseButtonImage(bottomButton);
                        isUpperPanelHidden = false;
                    }
                    break;
                default:
                    break;
            }
        }
        private void reverseButtonImage(Button button)
        {
            if ((string)((Image)button.Content).Tag == "down")
                button.Content = new Image() { Source = (ImageSource)Application.Current.FindResource("arrowUp"), Tag = "up" };
            else
                button.Content = new Image() { Source = (ImageSource)Application.Current.FindResource("arrowDown"), Tag = "down" };
        }

    }
}
